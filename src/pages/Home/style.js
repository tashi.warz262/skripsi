const style = {
  Utama:{
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    display: 'flex',
    '& div.konten': {
      width: '100%',
      backgroundColor: '#caf0f8',
      '& h1.informasiJob': {
        marginTop: '120px',
        textAlign: 'center',
        textTransform: 'uppercase',
      },
      '& .info':{
        textAlign: 'center',
        '& img':{
          width: '700px',
          height: '300px'
        },
        '& .modalbox':{
          display: 'inline-block',
          '& .box': {
            width: '600px',
            height: '268px',
            backgroundColor: 'white',
            borderRadius: '7px',
            boxShadow: 'rgba(0,0,0,0.35) 0px 5px 15px',
            marginBottom: '20px',
            '& .konten': {
              padding: '20px 50px',
              backgroundColor: 'white',
              borderRadius: '7px',
              '& .isi':{
                marginTop: '20px',
                textAlign: 'left',
                fontSize: '18px',
                fontWeight: 500,
                '& .subtitle':{
                  '& .kotak':{
                    '& .tanggal':{
                      '& td':{
                        paddingRight: '10px',
                        paddingBottom: '15px'
                      }
                    },
                    '& .jam':{
                      paddingBottom: '70px',
                      '& td':{
                        paddingRight: '10px',
                        paddingBottom: '15px'
                      }
                    }
                  }
                }
              }
            }
          }
        }
      },
      '& .box-informasi':{
        marginTop: '60px',
        marginBottom: '20px',
        textAlign: 'center',
        '& .informasi':{
          width: '800px',
          height: '150px',
          display: 'inline-block',
          '& .konten':{
            backgroundColor: 'white',
            display: 'flex',
            borderRadius: '5px',
            '& .statusLulus': {
              backgroundColor: '#17827B',
              width: '15px',
              height: '150px',
              borderTopLeftRadius: '5px',
              borderBottomLeftRadius: '5px',
              display: 'block',
              marginRight: '15px',
            },
            '& .statusGagal': {
              backgroundColor: 'red',
              width: '15px',
              height: '150px',
              borderTopLeftRadius: '5px',
              borderBottomLeftRadius: '5px',
              display: 'block',
              marginRight: '15px',
            },
            '& .tulisan':{
              textAlign: 'left',
              '& .title': {
                paddingTop: '20px',
              },
              '& .subtitle': {
                paddingRight: '20px',
                fontSize: '16px',
                paddingBottom: '20px',
              }
            }
          }
        }
      },
      '& .renderLamaran':{
        display:'flex',
        fontSize: '26px',
        justifyContent: 'center',
        marginTop: '60px',
        '& .steps':{
          display: 'flex',
          alignItems: 'center',
          '& .Index-Active':{
            backgroundColor: '#17827B',
            borderRadius: '100%',
            width: '50px',
            height: '50px',
            display: 'flex',
            color: 'white',
            paddingLeft: '17px',
            paddingTop: '4px',
            marginRight: '15px'
          },
          '& .Index-ActiveGagal':{
            backgroundColor: 'red',
            borderRadius: '100%',
            width: '50px',
            height: '50px',
            display: 'flex',
            color: 'white',
            paddingLeft: '17px',
            paddingTop: '4px',
            marginRight: '15px'
          },
          '& .Index-NonActive':{
            backgroundColor: '#9E9E9E',
            borderRadius: '100%',
            width: '50px',
            height: '50px',
            display: 'flex',
            color: 'white',
            paddingLeft: '17px',
            paddingTop: '4px',
            marginRight: '15px'
          },
          '& .Title-Active': {
            fontWeight: 500,
            marginRight: '10px',
          },
          '& .Title-ActiveGagal': {
            fontWeight: 500,
            marginRight: '10px',
            color: 'red'
          },
          '& .Title-NonActive': {
            fontWeight: 500,
            marginRight: '10px',
            color: '#9E9E9E'
          },
          '& .garis':{
            '& .active':{
              height: '3px',
              width: '50px',
              marginRight: '10px',
              opacity: '1'
            },
            '& .non-active':{
              height: '3px',
              width: '50px',
              marginRight: '10px',
              opacity: '1',
              backgroundColor: '#9E9E9E'
            }
          }
        }
      },
      '& div.renderKosongJob': {
        marginTop: '40px',
        width: '40%',
        justifyContent: 'center',
        textAlign: 'center',
        marginLeft: '520px',
        padding: '200px 0px 200px 140px'
      },
      '& div.renderJob': {
        marginTop: '40px',
        width: '100%',
        justifyContent: 'center',
        textAlign: 'center',
        '& div.box': {
          marginTop: '50px',
          marginLeft: '60px',
          width: '750px',
          height: '100%',
          padding: '25px',
          borderRadius: '15px',
          backgroundColor: 'white',
          boxShadow: 'rgba(0,0,0,0.35) 0px 5px 15px',
          display: 'inline-block',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          '& div.req': {
            textAlign: 'left',
            width: '100%',
            '& h4.judul': {
              height: '100%'
            }
          },
          '& div.reqdesc': {
            textAlign: 'left',
            width: '100%',
            marginTop: '20px',
            '& h4.judul': { 
              height: '100%'
            }
          },
          '& Button.daftar': {
            borderStyle: 'solid',
            borderRadius: '10%',
            backgroundColor: '#023e8a',
            color: 'white',
            fontWeight: '500',
            fontSize: '16px',
            width: '100px',
            height: '40px',
            cursor: 'pointer',
            '&:disabled':{
              cursor: 'not-allowed',
              backgroundColor: 'grey',
            }
          }
        }
      },
    },
  },
  ModalLogout: {
    width: '100%',
    height: '100%',
    backgroundColor: 'transparant',
    position: 'fixed',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 100,
    '& .isiKonten': {
      width: '500px',
      height: '250px',
      borderRadius: '12px',
      backgroundColor: 'white',
      boxShadow: 'rgba(0,0,0,0.35) 0px 5px 15px',
      display: 'flex',
      flexDirection: 'column',
      padding: '25px',
      justifyContent: 'center',
      alignItems: 'center',
      '& .Judul': {
        textAlign: 'center',
        '& h2.JudulError': {
          color: 'red'
        },
        '& h2.JudulSuccess': {
          color: '#19E121'
        },
      },
      '& .ButtonKonten': {
        marginTop: '40px',
        textAlign: 'center',
        '& Button.ButtonYa': {
          fontSize: '18px',
          cursor: 'pointer',
          borderStyle: 'solid',
          borderRadius: '10%',
          textAlign: 'capitalize',
          backgroundColor: '#19E121',
          color: 'white',
          height: '100%',
          width: '100px',
        },
        '& Button.ButtonTolak': {
          backgroundColor: 'red',
          marginLeft: '80px',
          fontSize: '18px',
          cursor: 'pointer',
          borderStyle: 'solid',
          borderRadius: '10%',
          textAlign: 'capitalize',
          color: 'white',
          height: '100%',
          width: '100px',
        }
      }
    }
  },
  KotakModal: {
    width: '100%',
    height: '100%',
    backgroundColor: 'transparant',
    position: 'fixed',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 99,
    '& .isiKonten': {
      width: '500px',
      height: '450px',
      borderRadius: '12px',
      backgroundColor: 'white',
      boxShadow: 'rgba(0,0,0,0.35) 0px 5px 15px',
      display: 'flex',
      flexDirection: 'column',
      padding: '5px 25px 25px',
      justifyContent: 'center',
      alignItems: 'center',
      '& .closeModal': {
        display: 'flex',
        justifyContent: 'flex-end',
        float: 'right',
        paddingLeft: '450px',
        '& Button': {
          backgroundColor: 'transparent',
          border: 'none',
          fontSize: '25px',
          cursor: 'pointer'
        }
      },
      '& .Judul': {
        textAlign: 'center',
      },
      '& .Konten': {
        textAlign: 'left',
        display: 'inline-block',
        width: '100%',
        height: '100%',
        marginTop: '30px',
        '& span.noted': {
          fontSize: '14px',
          fontWeight: '500',
          color: 'red',
        },
        '& span': {
          fontSize: '20px',
          fontWeight: '500',
          color: 'black',
        },
        '& input': {
          width: '100%',
          height: '40px',
          borderRadius: '4px',
          borderColor: 'black',
          borderWidth: '0.3px',
          fontSize: '16px',
        },
        '& p': {
          fontSize: '14px',
          color: 'red',
        },
      },
      '& .ButtonKonten': {
        marginTop: '40px',
        textAlign: 'center',
        '& Button': {
          fontSize: '18px',
          cursor: 'pointer',
          borderStyle: 'solid',
          borderRadius: '10%',
          textAlign: 'capitalize',
          backgroundColor: '#19E121',
          color: 'white',
          height: '100%',
          width: '100px',
          '&:disabled':{
            cursor: 'not-allowed',
            backgroundColor: 'grey'
          }
        }
      }
    }
  },
  UploadCV: {
    width: '100%',
    height: '100%',
    backgroundColor: 'transparant',
    position: 'fixed',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 99,
    '& .isiKonten': {
      width: '500px',
      height: '400px',
      borderRadius: '12px',
      backgroundColor: 'white',
      boxShadow: 'rgba(0,0,0,0.35) 0px 5px 15px',
      display: 'flex',
      flexDirection: 'column',
      padding: '5px 25px 25px',
      justifyContent: 'center',
      alignItems: 'center',
      '& .closeModal': {
        display: 'flex',
        justifyContent: 'flex-end',
        float: 'right',
        paddingLeft: '450px',
        '& Button': {
          backgroundColor: 'transparent',
          border: 'none',
          fontSize: '25px',
          cursor: 'pointer'
        }
      },
      '& .Judul': {
        textAlign: 'center',
      },
      '& .Konten': {
        textAlign: 'left',
        display: 'inline-block',
        width: '100%',
        height: '100%',
        marginTop: '30px',
        '& span.noted': {
          fontSize: '14px',
          fontWeight: '500',
          color: 'red',
        },
        '& span': {
          fontSize: '20px',
          fontWeight: '500',
          color: 'black',
        },
        '& input': {
          width: '100%',
          height: '40px',
          borderRadius: '4px',
          borderColor: 'black',
          borderWidth: '0.3px',
          fontSize: '16px',
        },
        '& p': {
          fontSize: '14px',
          color: 'red',
        },
      },
      '& .ButtonKonten': {
        marginTop: '40px',
        textAlign: 'center',
        '& Button': {
          fontSize: '18px',
          cursor: 'pointer',
          borderStyle: 'solid',
          borderRadius: '10%',
          textAlign: 'capitalize',
          backgroundColor: '#19E121',
          color: 'white',
          height: '100%',
          width: '100px',
          '&:disabled':{
            cursor: 'not-allowed',
            backgroundColor: 'grey'
          }
        }
      }
    }
  },
  header: {
    backgroundColor: 'white',
    position: 'fixed',
    display: 'flex',
    width: '100%',
    height: '90px',
    zIndex: '10',
    justifyContent: 'center',
    '& .leftcontent': {
      marginRight: '700px',
      '& .logo':{
        height: '6em',
        marginRight: '30px'
      },
    },
  },
  footer: {
    backgroundColor: 'black',
    justifyContent: 'center',
    textAlign: 'center',
    '& .info':{
      color: 'white',
      fontSize: '18px',
    }
  },
  Name: {
    marginTop: '25px',
    marginRight: '30px',
    fontSize: '20px',
    textTransform: 'capitalize',
    color: 'black',
  },
  ButtonLogout: {
    marginTop: '25px',
    borderStyle: 'solid',
    borderRadius: '10%',
    backgroundColor: 'red',
    color: 'white',
    textTransform: 'capitalize',
    fontWeight: '500',
    fontSize: '16px',
    width: '100px',
    height: '40px',
    cursor: 'pointer',
    marginRight: '50px',
  },
  
  ButtonRegister: {
    marginTop: '25px',
    borderStyle: 'solid',
    borderRadius: '10%',
    backgroundColor: '#19E121',
    color: 'white',
    fontWeight: '500',
    fontSize: '16px',
    width: '100px',
    height: '40px',
    cursor: 'pointer',
    marginRight: '50px',
  },
  ButtonLogin: {
    marginTop: '25px',
    borderStyle: 'solid',
    borderRadius: '10%',
    backgroundColor: 'white',
    color: '#19E121',
    fontWeight: '500',
    fontSize: '16px',
    width: '100px',
    height: '40px',
    cursor: 'pointer',
    marginRight: '50px',
  },
  SubMenu: {
    paddingTop: '15px',
    cursor: 'pointer',
    fontWeight: '700',
    fontSize: '18px',
    height: '100%',
    position: 'absolute',
    '& .subtitle1': {
      padding: '20px 15px 30px',
      borderBottom: '1px solid black',
      '&:hover': {
        color: '#19E121',
        borderBottom: '1px solid #19E121'
      },
    },
    '& .subtitle2': {
        padding: '20px 15px 30px',
      borderBottom: '1px solid black',
      '&:hover': {
        color: '#19E121',
        borderBottom: '1px solid #19E121'
      },
    }
  },
};

export default style;