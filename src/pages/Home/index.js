import Home from './Home';
import styles from './style';
import withStyles from 'react-jss';
import { withRouter } from 'next/router';
import { connect } from 'react-redux';
import * as actions from '../../constants/actions';
import { bindActionCreators } from 'redux';

const Styled = withStyles(styles)(Home);
const Routered = withRouter(Styled);
const mapStateToProps = ({ user, error, Job, message, progress }) => ({ user, error, Job, message, progress });
const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(actions, dispatch)
});
export default connect(mapStateToProps, mapDispatchToProps)(Routered);