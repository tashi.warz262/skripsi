import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'antd';
import { isNil } from 'ramda';
import { Header, Footer } from 'antd/lib/layout/layout';
import moment from 'moment-timezone';
import get from 'lodash.get';

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ModalLogin: false,
      ModalRegis: false,
      ModalLogout: false,
      ModalError: false,
      ModalSuccess: false,
      ModalRenderJob: false,
      ModalDaftar: false,
      ModalGagalDaftar: false,
      listJob: {},
      email: null,
      password: null,
      username: null,
      CV: '',
      idJobDaftar: null,
      daftar: false,
      konten: 0,
      steps: [
        {
          index: 1,
          title: 'Kirim CV',
          description: ''
        },
        {
          index: 2,
          title: 'Tes Kemampuan',
          description: ''
        },
        {
          index: 3,
          title: 'Wawancara HR',
          description: ''
        },
        {
          index: 4,
          title: 'Pengumuman Hasil',
          description: ''
        }
      ]
    }
  }
  componentDidMount () {
    const { actions } = this.props;
    actions.getJob();
    if(window.sessionStorage.getItem('username') !== null) this._setUser();
  }

  componentDidUpdate(prevProps) {
    const { ModalRegis, ModalLogin, ModalDaftar } = this.state;
    const { error, message } = this.props
    if(error !== prevProps.error && !isNil(error) && (ModalLogin || ModalRegis || ModalDaftar)) this._ModalError();
    if(message !== prevProps.message && !isNil(message) && (ModalLogin || ModalRegis || ModalDaftar)) this._ModalSuccess();
    if(this.props.Job !== prevProps.Job) this._setJob();
    if(this.props.user !== prevProps.user && this.props.user !== null) this._getLamaran();
  }

  _setUser = () => {
    let name = window.sessionStorage.getItem('username');
    let id = window.sessionStorage.getItem('userId');
    let tkn = window.sessionStorage.getItem('token');
    let payload = { userId: id, username: name, token: tkn };
    this.props.actions.setUser(payload);
  }

  _setJob = () => {
    const { Job } = this.props;
    this.setState({ listJob: Job, ModalRenderJob: true });
  }

  _getLamaran = () => {
    const { user: { userId, token } } = this.props;
    this.props.actions.getLowongan(userId, token);
  }

  _getinfo = () => {
    const { user, actions } = this.props;
    actions.getJob();
    if(user !== undefined && user !== null) actions.getLowongan(user.userId, user.token);
  }

  _closeModalError = () => {
    const { actions } = this.props;
    actions.resetError();
    this.setState({ ModalError: false });
  }

  _closeModalSuccess = () => {
    const{ user } = this.props;
    const { Daftar } = this.state;
    this.props.actions.resetError();
    this.setState({ ModalSuccess: false });
    if(window.sessionStorage.getItem('user') === null && this.props.user !== null) {
      window.sessionStorage.setItem('userId', user.userId);
      window.sessionStorage.setItem('username', user.username);
      window.sessionStorage.setItem('token', user.token);
    }
    if(Daftar && user) {
      this.props.actions.getLowongan(user.userId, user.token);
      this.setState({ Daftar: false });
    }
  }

  _ModalError = () => {
    this.setState({ ModalError: true });
  }

  _ModalSuccess = () => {
    const { ModalDaftar } = this.state;
    if(ModalDaftar) this.setState({ Daftar: true });
    this._closeModal();
    this.setState({ ModalSuccess: true });
  }

  _LoginRequest = () => {
    const { email, password } = this.state;
    const { actions } = this.props;
    let user = { email, password };
    actions.loginUser(user);
  }

  _RegisRequest = () => {
    const { username, email, password } = this.state;
    const { actions } = this.props;
    let user = { username, email, password };
    actions.registerUser(user);
  }

  _logoutRequest = () => {
    const { actions } = this.props;
    actions.logoutUser();
    this.setState({ ModalLogout: false });
    window.sessionStorage.clear();
  }

  _setValue = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  _setFile = (e) => {
    const file = e.target.files[0];
    this.setState({ [e.target.name]: file });
  }

  _RegisModal = () => {
    this.setState({ ModalRegis: true });
  }

  _LoginModal = () => {
    this.setState({ ModalLogin: true });
  }

  _LogoutModal = () => {
    this.setState({ ModalLogout: true });
  }

  _closeModal = () => {
    this.setState({ ModalLogin: false, ModalRegis: false, ModalLogout: false, ModalDaftar: false, username: '', email: '', password: '', idJobDaftar: null, nomor: '', cv: null });
  }

  _closeModalGagal = () => {
    this.setState({ ModalGagalDaftar: false });
  }

  _daftarJob = (idJob) => {
    const { user } = this.props;
    if(!user) this.setState({ ModalGagalDaftar: true });
    if(user) this.setState({ idJobDaftar: idJob, ModalDaftar: true });
  }

  _registerJob = () => {
    const data = new FormData();  
    const { idJobDaftar, CV } = this.state;
    const { actions, user: { token } } = this.props;
    const id = idJobDaftar;
    data.append('cvFile', CV);
    actions.daftarJob(token, data, id);
  }

  _renderLowonganJob = () => {
    const { listJob, ModalRegis, ModalLogin, ModalLogout } = this.state;
    const length = listJob.length;
    const currentTime = moment(new Date().toISOString()).format();
    const today = moment.tz(currentTime, 'Asia/Jakarta').format('YYYY/MM/DD');
    if(length !== 0) {
      return listJob.map((data, index) => {
        return (
          <div id={index} className="box">
            <h2 className="title">{data.jobTitle}</h2>
            <div className="req">
              <h4 className="judul">Deskripsi Pekerjaan:</h4>
              <span className="desc">{data.jobDescription}</span>
            </div>
            <div className="reqdesc">
              <h4 className="judul">Spesifikasi Pekerjaan:</h4>
              <span className="desc">{data.jobRequirements}</span>
              <br/><br/><h6 className="desc">Tanggal Dibuka: <span className="desc">{moment(data.startDate).format('DD/MM/YYYY')}</span></h6><h6 className="desc">Tanggal Ditutup: <span className="desc">{moment(data.endDate).format('DD/MM/YYYY')}</span></h6><br/>
            </div>
            <Button className="daftar" onClick={this._daftarJob.bind(this, data._id)} disabled={ModalLogin || ModalRegis || ModalLogout || moment(today).isAfter(moment(data.endDate).format('YYYY/MM/DD'))}>Daftar</Button>
          </div>
        );
      })
    }
  }

  _renderLamaran = () => {
    const { steps } = this.state;
    const { progress } = this.props;
    let length = 4;
    return (
      steps.map(data => {
        return(
          <div className='steps'>
            {(get(progress, '0.progressStatus_code.0') === 4 && get(progress, '0.status')  === 'Gagal') ? <span className={get(progress, '0.progressStatus_code.1') >= data.index ? 'Index-ActiveGagal' : 'Index-NonActive'}>{data.index}</span> : <span className={get(progress, '0.progressStatus_code.0') >= data.index ? 'Index-Active' : 'Index-NonActive'}>{data.index}</span>}
            {(get(progress, '0.progressStatus_code.0') === 4 && get(progress, '0.status')  === 'Gagal') ? <span className={get(progress, '0.progressStatus_code.1') >= data.index ? 'Title-ActiveGagal' : 'Title-NonActive'}>{data.title}</span> : <span className={get(progress, '0.progressStatus_code.0') >= data.index ? 'Title-Active' : 'Title-NonActive'}>{data.title}</span>}
            {length !== data.index ? ((get(progress, '0.progressStatus_code.0') === 4 && get(progress, '0.status')  === 'Gagal') ? <span className='garis'><hr className={get(progress, '0.progressStatus_code.1') >= data.index ? 'active' : 'non-active'}/></span> : <span className='garis'><hr className={get(progress, '0.progressStatus_code.0') >= data.index ? 'active' : 'non-active'}/></span>) : ''}
          </div>
        )
      })
    )
  }

  _renderIsiLamaran = () => {
    const { progress } = this.props;
    return(
      <div className='box-informasi'>
        <div className='informasi'>
          <div className='konten'>
            {get(progress, '0.progressStatus_code.0') !== 4 && <span className='statusLulus'/>}
            {get(progress, '0.progressStatus_code.0') === 4 && <span className='statusGagal'/>}
            <div className='tulisan'>
              {get(progress, '0.progressStatus_code.0') !== 4 && <h3 className='title'>{get(progress, '0.progressStatus_code.0') === 1 ? 'Sedang Diproses' : `Selamat Anda Memasuki Tahap - ${get(progress, '0.progressStatus_code.0')}`}</h3>}
              {get(progress, '0.progressStatus_code.0') === 4 && <h3 className='title'>{(get(progress, '0.progressStatus_code.0') === 4 && get(progress, '0.status') === 'Gagal') ? 'Ditolak' : 'Selamat Anda Masuk Tahap Terakhir'}</h3>}
              {get(progress, '0.progressStatus_code.0') === 1 && <p className='subtitle'>Curriculum Vitae kamu sedang kami proses, silahkan menunggu hasil seleksi Curriculum Vitae Anda dengan cara cek website kami secara berkala. Terima Kasih</p>}
              {get(progress, '0.progressStatus_code.0') === 2 && <p className='subtitle'>Selamat Kamu berhasil cv kamu berhasil terpilih dan memasuki tahap tes kemampuan. Dibawah merupakan jadwal untuk mengikuti tes kemampuannya dan jangan lupa mempersiapkannya.</p>}
              {get(progress, '0.progressStatus_code.0') === 3 && <p className='subtitle'>Selamat Kamu berhasil lolos pada tahap tes kemampuan dan memasuki tahap wawancara dengan HR. Dibawah merupakan jadwal wawancara kamu. Jangan lupa untuk berpenampilan rapih dan sopan.</p>}
              {get(progress, '0.progressStatus_code.0') === 4 && <p className='subtitle'>{(get(progress, '0.progressStatus_code.0') === 4 && get(progress, '0.status') === 'Gagal') ? 'Mohon Maaf kamu belum berhasil mengikuti lamaran ini. Silahkan untuk coba kembali' : 'Selamat kamu berhasil lolos pada tahap akhir untuk melakukan wawancara dengan atasan. Dibawah ini merupakan jadwal wawancara kamu. Jangan lupa untuk berpenampilan rapih dan sopan.'}</p>}
            </div>
          </div>
        </div>
      </div>
    )
  }

  _renderInformasi = () => {
    const { progress } = this.props;
    return(
      <div className='info'>
        {(get(progress, '0.progressStatus_code.0') === 2 || get(progress, '0.progressStatus_code.0') === 3 || (get(progress, '0.progressStatus_code.0') === 4 && get(progress, '0.status')  !== 'Gagal')) && <div className='modalbox'>
          <div className='box'>
            <div className='konten'>
              <h4 className='Title'>{get(progress, '0.progressStatus_code.0') === 2 ? 'Jadwal Mengikuti Test' : 'Jadwal Wawancara'}</h4>
              <div className='isi'>
                <p className='subtitle'>
                  <table className='kotak'>
                    <tr className='tanggal'>
                      <td>Pada Tanggal </td>
                      <td>:</td>
                      <td>{moment(get(progress, '0.datetime')).format('dddd') + ', ' + moment(get(progress, '0.datetime')).format('Do MMMM YYYY')}</td>
                    </tr>
                    <tr className='jam'>
                      <td>Pada Jam </td>
                      <td>:</td>
                      <td>{moment(get(progress, '0.datetime')).format('LT')}</td>
                    </tr>
                    <tr className='lokasi'>
                      <td>Pada Lokasi </td>
                      <td>:</td>
                      <td>{get(progress, '0.location')}</td>
                    </tr>
                  </table>
                </p>
              </div>
            </div>
          </div>
        </div>}
      </div>
    )
  }

  _renderKosongLowonganJob = () => {
    const { listJob, konten } = this.state;
    if(listJob.length === 0 || (listJob.length !== 0 && konten === 0)){
      return (
        <div>
          <h1>Mohon Maaf untuk saat ini belum ada lowongan pekerjaan yang sedang dibuka.<br/>Terima Kasih</h1>
        </div>
      )
    }
  }

  render(){
    const { user, classes, error, message, progress } = this.props;
    const { ModalLogin, ModalLogout, ModalRegis, ModalError, ModalSuccess, ModalRenderJob, ModalDaftar, ModalGagalDaftar, username, email, password, CV, listJob } = this.state;
    return(
      <div>
        {ModalLogin && <div className={classes.KotakModal}>
            <div className="isiKonten">
              <div className={'closeModal'}>
                <Button onClick={this._closeModal}>x</Button>
              </div>
              <div class="Judul">
                <h2>Selamat Datang<br/>Silahkan Login Terlebih Dahulu</h2>
              </div>
              <div className="Konten">
                <span>Email:</span><br/>
                <input placeholder="Email" name="email" type="email" value={email} onChange={this._setValue}/>
                <br/><br/><span>Password:</span><br/>
                <input placeholder="Password" type="password" name="password" value={password} onChange={this._setValue} />
              </div>
              <div className="ButtonKonten">
                <Button onClick={this._LoginRequest} disabled={email === undefined || email === null || email === '' || password === undefined || password === null || password === ''}>Login</Button>
              </div>
            </div>
          </div>}
          {ModalRegis && <div className={classes.KotakModal}>
            <div className="isiKonten">
              <div className={'closeModal'}>
                <Button onClick={this._closeModal}>x</Button>
              </div>
              <div className="Judul">
                <h3>Selamat Datang<br/>Silahkan Registrasi Terlebih Dahulu</h3>
              </div>
              <div className="Konten">
                <span>Nama Lengkap:</span><br/>
                <input placeholder="Nama Lengkap" name="username" type="text" value={username} onChange={this._setValue}/>
                <br/><span>Email:</span><br/>
                <input placeholder="Email" name="email" type="email" value={email} onChange={this._setValue}/>
                <br/><span>Password:</span><br/>
                <input placeholder="Password" type="password" name="password" value={password} onChange={this._setValue} />
              </div>
              <div className="ButtonKonten">
                <Button onClick={this._RegisRequest} disabled={username === undefined || username === null || username === '' ||
                email === undefined || email === null || email === '' || password === undefined || password === null || password === '' }>Register</Button>
              </div>
            </div>
          </div>}
          {ModalLogout && <div className={classes.ModalLogout}>
            <div className="isiKonten">
              <div className="Judul">
                <h2>Apakah Anda yakin ingin Logout?</h2>
              </div>
              <div className="ButtonKonten">
                <Button onClick={this._logoutRequest} className="ButtonYa">Ya</Button>
                <Button onClick={this._closeModal} className="ButtonTolak">Tidak</Button>
              </div>
            </div>
          </div>}
          {ModalError && <div className={classes.ModalLogout}>
            <div className="isiKonten">
              <div className="Judul">
                <h2 className="JudulError">{error}</h2>
              </div>
              <div className="ButtonKonten">
                <Button onClick={this._closeModalError} className="ButtonYa">Ok</Button>
              </div>
            </div>
          </div>}
          {ModalSuccess && <div className={classes.ModalLogout}>
            <div className="isiKonten">
              <div className="Judul">
                <h2 className="JudulSuccess">{message}</h2>
              </div>
              <div className="ButtonKonten">
                <Button onClick={this._closeModalSuccess} className="ButtonYa">Ok</Button>
              </div>
            </div>
          </div>}
          {ModalGagalDaftar && <div className={classes.ModalLogout}>
            <div className="isiKonten">
              <div className="Judul">
                <h2 className="JudulError">Silahkan Login Terlebih Dahulu!</h2>
              </div>
              <div className="ButtonKonten">
                <Button onClick={this._closeModalGagal} className="ButtonYa">Ok</Button>
              </div>
            </div>
          </div>}
          {ModalDaftar && <div className={classes.UploadCV}>
            <div className="isiKonten">
              <div className={'closeModal'}>
                <Button onClick={this._closeModal}>x</Button>
              </div>
              <div className="Judul">
                <h3>Silahkan Mengunggah File CV Anda</h3>
              </div>
              <form>
                <div className="Konten">
                  <span>Upload File Curriculum Vitae:</span><br/>
                  <input name="CV" type="file" onChange={this._setFile} accept="pdf" filename={CV} />
                  <span className="noted">*Size file maksimum 2 Mb dan File harus dengan format pdf</span>
                </div>
              </form>
              <div className="ButtonKonten">
                <Button onClick={this._registerJob} disabled={CV === undefined || CV === null || CV === ''}>Simpan</Button>
              </div>
            </div>
          </div>}
        <Header className={classes.header}> 
          <div className="leftcontent">
            <img src='FrisianFlag.png' rel="icon" alt="logo" class="logo" />
            <span className={classes.SubMenu}>
              <span className="subtitle1" onClick={this._getinfo}>{progress !== undefined && progress !== null && progress.length !== 0 ? 'Lamaran Saya' : 'Informasi Kerja'}</span>
            </span>
          </div>
          {isNil(user) || user === undefined ?
          <div className="rightcontent">
            <Button className={classes.ButtonRegister} onClick={this._RegisModal} disabled={ModalLogin || ModalRegis}>Register</Button>
            <Button className={classes.ButtonLogin} onClick={this._LoginModal} disabled={ModalLogin || ModalRegis}>Login</Button>
          </div> :
          <div class="rightcontent">
            <span className={classes.Name}>Hi, {user.username}</span>
            <Button className={classes.ButtonLogout} onClick={this._LogoutModal} disabled={ModalLogout}>Logout</Button>
          </div>}
        </Header>
        <div className={classes.Utama}>
          <div className="konten">
            <h1 className="informasiJob">{progress !== undefined && progress !== null &&progress.length !== 0 ? 'Informasi Lamaran Saya' : 'Informasi Lowongan Kerja'}</h1>
            {progress !== undefined && progress !== null &&progress.length !== 0 ?
            <div className="renderLamaran">{this._renderLamaran()}</div>
            : <div className="renderJob">{ModalRenderJob && this._renderLowonganJob()}</div>}
            {progress !== undefined && progress !== null && progress.length !== 0 && this._renderIsiLamaran()}
            {progress !== undefined && progress !== null && progress.length !== 0 && this._renderInformasi()}
            {(progress === undefined || progress === null || progress.length === 0) && <div className="renderKosongJob">{listJob.length === 0 && this._renderKosongLowonganJob()}</div>}
          </div>
        </div>
        <Footer className={classes.footer}>
          <div className="info">
            PT. Frisian Flag Bandung<br/>Alamat: Jl. Batununggal Mulia I, Mengger, Kec. Bandung Kidul,<br/>Kota Bandung, Jawa Barat 40267<br/>Tlp: (022) 7506990
          </div>
        </Footer>
      </div>
    )
  }
}

Home.propTypes = {
  user: PropTypes.object.isRequired,
};