export const baseUrl = ' https://peaceful-chamber-49198.herokuapp.com';

export const endpoints = {
  getJob: baseUrl + '/user/v1/job-vacancy',
  loginUser: baseUrl + '/user/v1/login',
  daftarJob: (id) => baseUrl + `/user/v1/job-applications/${id}`,
  getLowongan: (id) => baseUrl + `/user/v1/job-applications/${id}`,
  registerUser: baseUrl + '/user/v1/register'
}