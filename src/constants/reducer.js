import { AUTH_ERROR, AUTH_LOGIN, AUTH_REGISTER, GET_JOB, AUTH_LOGOUT, RESET_ERROR, AUTH_DAFTAR_JOB, AUTH_LAMARAN, SET_USER } from "./type";

const initState = {
    user: null,
    error: null,
    Job: null,
    message: null,
    progress: null,
}

const rootReducer = (state = initState, action) => {
    const { type, payload, error, message } = action;
    switch(type) {
      case 'reset':
        return initState;
      case AUTH_LOGIN:
        return {
          ...state,
          user: payload,
          message
        };
      case AUTH_DAFTAR_JOB:
        return {
          ...state,
          message
        }
      case AUTH_REGISTER:
        return {
          ...state,
          message
        };
      case AUTH_LOGOUT:
        return {
          ...state,
          user: null,
          error: null,
          message: null,
          progress: null,
        };
      case GET_JOB:
        return {
          ...state,
          Job: payload
        };
      case AUTH_LAMARAN:
        return {
          ...state,
          progress: payload
        }
      case AUTH_ERROR:
        return {
          ...state,
          error
        };
      case RESET_ERROR:
        return {
          ...state,
          error: null,
          message: null
        };
      case SET_USER:
        return {
          ...state,
          user: payload
        }
      default:
        return state;
    }
}

export default rootReducer;