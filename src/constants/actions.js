import axios from 'axios';
import { AUTH_ERROR, AUTH_LOGIN, AUTH_REGISTER, AUTH_LOGOUT, GET_JOB, RESET_ERROR, AUTH_DAFTAR_JOB, AUTH_LAMARAN, SET_USER } from './type';
import { endpoints } from './api';

export const registerUser = (user) => dispatch => {
    axios.post(endpoints.registerUser, user)
            .then(res => dispatch({
                type: AUTH_REGISTER,
                message: res.data.message
            }))
            .catch(err => {
                if(err.response.status === 400){
                  dispatch({
                      type: AUTH_ERROR,
                      error: err.response.data.message
                  })
                }
                else if(err.response.status === 409){
                  dispatch({
                      type: AUTH_ERROR,
                      error: err.response.data.message
                  })
                }
            })
}

export const daftarJob = (token, payload, id) => dispatch => {
    const config = { headers: { Authorization: `Bearer ${token}` } };
    axios.post(endpoints.daftarJob(id), payload, config )   
    .then(res => dispatch({ 
        type: AUTH_DAFTAR_JOB,
        message: res.data.message
    }))
    .catch(err => {
      if(err.response.status === 400){
        dispatch({
            type: AUTH_ERROR,
            error: err.response.data.message
        })
      }
      else if(err.response.status === 409){
        dispatch({
            type: AUTH_ERROR,
            error: err.response.data.message
        })
      }
    })
}

export const loginUser = (user) => dispatch => {
    axios.post(endpoints.loginUser, user)
            .then(res => {
                dispatch({
                    type: AUTH_LOGIN,
                    payload: res.data.user,
                    message: res.data.message
                })
            })
            .catch(err => {
                dispatch({
                    type: AUTH_ERROR,
                    error: err.response.data.message
                })
            });
}

export const getLowongan = (id, token) => dispatch => {
  const config = { headers: { Authorization: `Bearer ${token}` } };
  axios.get(endpoints.getLowongan(id), config).then(
    res => dispatch({
      type: AUTH_LAMARAN,
      payload: res.data.data
    }))
    .catch(err => {dispatch({
      type: AUTH_ERROR,
      error: err.response.data.message
    })})
}

export const getJob = () => dispatch => {
    axios.get(endpoints.getJob).then(
        res => {
            dispatch({
                type: GET_JOB,
                payload: res.data.data
            })
        }
    ).catch(err => {
        dispatch({
            type: AUTH_ERROR,
            error: err.response.data.message
        })
    })
}

export const logoutUser = () => {
    return {
        type: AUTH_LOGOUT
    }
}

export const resetError = () => {
    return{
        type: RESET_ERROR
    }
}

export const setUser = (payload) => dispatch => {
    dispatch({
        type: SET_USER,
        payload
    })
}