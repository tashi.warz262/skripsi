export const AUTH_LOGIN = 'AUTH_LOGIN';
export const AUTH_REGISTER = 'AUTH_REGIS';
export const AUTH_ERROR = 'AUTH_ERROR';
export const AUTH_LOGOUT = 'AUTH_LOGOUT';
export const GET_JOB = 'GET_JOB';
export const AUTH_LAMARAN = 'AUTH_LAMARAN';
export const RESET_ERROR = 'RESET_ERROR';
export const AUTH_DAFTAR_JOB = 'AUTH_DAFTAR_JOB';
export const SET_USER = 'SET_USER';