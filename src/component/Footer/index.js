import styles from './Footer.module.css'

export default function Footer() {
  return (
    <footer className={styles.footer}>
      <div className={styles.info}>PT. Frisian Flag Bandung<br/>Alamat: Jl. Batununggal Mulia I, Mengger, Kec. Bandung Kidul,<br/>Kota Bandung, Jawa Barat 40267<br/>Tlp: (022) 7506990
      </div>
    </footer>
  )
}
