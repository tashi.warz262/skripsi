import React, { Component } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Home from './pages/Home';
import history from './constants/history';
import { Provider } from 'react-redux';
import store from './constants/store';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'antd/dist/antd.css';

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Provider store = {store}>
        <Router history = {history}>
            <Routes>
              <Route path="/" Component={Home} element={<Home />} />
            </Routes>
          </Router>
        </Provider>
      </React.Fragment>
    );
  }
}

export default App;
